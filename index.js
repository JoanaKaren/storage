function guardarEnSesionStorage() {
    var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
    var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
    var clave = txtClave.value;
    var valor = txtValor.value;
    sessionStorage.setItem(clave, valor);
    var objeto = {
      nombre:"Joana",
      apellidos:"Melgarejo Aguirre",
      ciudad:"Lima",
      pais:"Perú"
    };
    sessionStorage.setItem("json", JSON.stringify(objeto));
    document.getElementById("txtClave").value="";
    document.getElementById("txtValor").value="";
  }
  function leerDeSessionStorage() {
    var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
    var clave = txtClave.value;
    var valor = sessionStorage.getItem(clave);
    var spanValor = document.getElementById("spanValor");
    spanValor.innerText = valor;

    var datosUsuario = JSON.parse(localStorage.getItem("json"));
    console.log(datosUsuario.nombre);
    console.log(datosUsuario.pais);
    console.log(datosUsuario);
  }

  function EliminarEnSesionStorage() {
    var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
    var clave = txtClave.value;
    var valor = sessionStorage.removeItem(clave);
    spanValor.innerText = "Session eliminado :" + clave;
  }

  function ClearEnSesionStorage() {
    sessionStorage.clear();
    var spanValor = document.getElementById("spanValor");
    spanValor.innerText = "Se limpio todos los datos de sessionStorage ";
  }

 function LenghEnSesionStorage() {
   var numElementSaved = sessionStorage.length;
   var spanValor = document.getElementById("spanValor");
   spanValor.innerText = "Número de elementos guardados en sessión : " + numElementSaved;
  }
